package com.myvlille.myvlilleapi.controller

import com.myvlille.myvlilleapi.service.StationService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class StationController(
        private val stationService: StationService
) {

    @GetMapping(value="/stations", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getStations() = stationService.getAllStations()

    @GetMapping(value="/stations/closest/{lat}/{long}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getClosestStations(@PathVariable("lat") latitude: Double, @PathVariable("long")longitude: Double) = stationService.getAllStationsSortedByLocation(latitude, longitude)
}