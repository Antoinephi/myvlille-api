package com.myvlille.myvlilleapi.dto

import java.time.LocalDateTime

data class StationDto(
        val city: String,
        val availableBikes: Int,
        val availableSlots: Int,
        val coordinates: Coordinates,
        val lastUpdated: LocalDateTime,
        val address: String,
        val hasPaymentCard: Boolean,
        val name: String,
        val isOnline: Boolean,
        val id: Int
        )

data class Coordinates(val latitude: Double, val longitude: Double)

const val IS_STATION_IN_ORDER = "EN SERVICE"
const val HAS_PAYMENT_CARD = "AVEC TPE"