package com.myvlille.myvlilleapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching


@SpringBootApplication
@EnableCaching
class MyVlilleApiApplication

fun main(args: Array<String>) {
	runApplication<MyVlilleApiApplication>(*args)
}

