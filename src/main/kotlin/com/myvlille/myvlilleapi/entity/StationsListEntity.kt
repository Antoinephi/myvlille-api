package com.myvlille.myvlilleapi.entity

data class StationsListEntity(val records: List<RawStationEntity>)

data class RawStationEntity(val fields: StationEntity)

data class StationEntity(
        val etat: String,
        val commune: String,
        val geo: List<Double>,
        val nbvelosdispo: Int,
        val nbplacesdispo: Int,
        val nom: String,
        val etatconnexion: String,
        val libelle: Int,
        val datemiseajour: String,
        val adresse: String,
        val type: String
)