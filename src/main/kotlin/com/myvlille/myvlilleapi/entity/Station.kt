package com.myvlille.myvlilleapi.entity

import java.time.LocalDateTime

data class Station(var id: Long,
                   var name: String,
                   var availableBikes: Integer,
                   var availableSlots: Integer,
                   var state: String,
                   var type: String,
                   var lastUpdated: LocalDateTime,
                   var latitude: Long,
                   var longitude: Long)