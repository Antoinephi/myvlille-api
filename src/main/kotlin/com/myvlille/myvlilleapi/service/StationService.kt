package com.myvlille.myvlilleapi.service

import com.myvlille.myvlilleapi.dto.Coordinates
import com.myvlille.myvlilleapi.dto.HAS_PAYMENT_CARD
import com.myvlille.myvlilleapi.dto.IS_STATION_IN_ORDER
import com.myvlille.myvlilleapi.dto.StationDto
import com.myvlille.myvlilleapi.entity.StationEntity
import com.myvlille.myvlilleapi.entity.StationsListEntity
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.time.ZonedDateTime
import javax.annotation.Resource

@Service
@CacheConfig(cacheNames = ["stations"])
class StationService {

    @Resource
    private lateinit var self: StationService

    private var restTemplate = RestTemplate()
    private var apiUrl: String = "https://opendata.lillemetropole.fr/api/records/1.0/search/?dataset=vlille-realtime&rows=300&facet=libelle&facet=nom&facet=commune&facet=etat&facet=type&facet=etatconnexion"

    @Cacheable()
    fun getAllStations(): List<StationDto>? {
        try {
            println("waiting...")
            Thread.sleep(3000L)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        var response = restTemplate.getForEntity(apiUrl, StationsListEntity::class.java)
        val body = response.body
        return body?.records?.map { it -> mapEntityToDto(it.fields) }
    }

    fun getAllStationsSortedByLocation(latitude: Double, longitude: Double): List<StationDto>? {
        println(latitude.toString() + " " + longitude.toString())
        return self.getAllStations()?.sortedBy { it ->distance(latitude, it.coordinates.latitude, longitude, it.coordinates.longitude, 0.0, 0.0)}
    }

    fun mapEntityToDto(stationEntity: StationEntity): StationDto {
        return StationDto(
                stationEntity.commune,
                stationEntity.nbvelosdispo,
                stationEntity.nbplacesdispo,
                Coordinates(stationEntity.geo[0], stationEntity.geo[1]),
                ZonedDateTime.parse(stationEntity.datemiseajour).toLocalDateTime(),
                stationEntity.adresse,
                stationEntity.type == HAS_PAYMENT_CARD,
                stationEntity.nom,
                stationEntity.etat == IS_STATION_IN_ORDER,
                stationEntity.libelle
        )
    }

    fun distance(lat1: Double, lat2: Double, lon1: Double,
                 lon2: Double, el1: Double, el2: Double): Double {

        val R = 6371 // Radius of the earth

        val latDistance = Math.toRadians(lat2 - lat1)
        val lonDistance = Math.toRadians(lon2 - lon1)
        val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        var distance = R.toDouble() * c * 1000.0 // convert to meters

        val height = el1 - el2

        distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)

        return Math.sqrt(distance)
    }

}